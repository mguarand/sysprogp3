#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int validarEscritura(char *nombre);
int validarEdad(int edad);
void main(){
  char c;
  float promEdad=0;
  int sumaEdad=0;
  int vez=1;
  char *nombreViejo=(char*) malloc( 100 );
  char *nombreJoven=(char*) malloc( 100 );
  char *apellidoJoven=(char*) malloc( 100 );
  char *apellidoViejo=(char*) malloc( 100 );
  int minEdad=131;
  int maxEdad=0;
  while(vez<11){
    char *nombre=(char*) malloc( 100 );
    char *apellido=(char*) malloc( 100 );
    int edad;
    printf("Persona %d \n",vez);
    printf("Ingrese su nombre: ");
    gets(nombre);
    while(validarEscritura(nombre)==0){
      printf("\n");
      printf("Ingrese su nombre: ");
      gets(nombre);
    }
    printf("\n");
    printf("Ingrese su apellido: ");
    gets(apellido);
    while(validarEscritura(apellido)==0){
      printf("\n");
      printf("Ingrese su apellido:");
      gets(apellido);
    }
    printf("\n");
    printf("Ingrese su edad: ");
    
    while (((scanf("%d%c", &edad, &c)!=2 || c!='\n') && clean_stdin()) || validarEdad(edad)==0){
      printf("\nNo es un número o está incorrecto. Ingrese su edad: ");
     
    }
    if(edad<minEdad){
      minEdad=edad;
      nombreJoven=nombre;
      apellidoJoven=apellido;
    }
    if(edad>maxEdad){
      maxEdad=edad;
      nombreViejo=nombre;
      apellidoViejo=apellido;
    }
    sumaEdad+=edad;
    vez+=1;
  }
  promEdad=sumaEdad/10.0;
  printf("Promedio de edad = %.2f \n",promEdad);
  printf("Persona más vieja: %s %s \n",nombreViejo,apellidoViejo);
  printf("Persona más joven: %s %s \n",nombreJoven,apellidoJoven);
}

int validarEscritura(char *nombre){
  int contador=1;
  if(isalpha(*nombre)==0||islower(*nombre)!=0){
    printf("Debe empezar con mayúscula  \n");
    return 0;
  }
  nombre+=1;
  while(*nombre!='\0'){
    
    if(isalpha(*nombre)==0 || islower(*nombre)==0){
      printf("No debe ser un caracter especial o dígito o mayúscula \n");
      return 0;
    }
    nombre++;  
    contador++;  
  }
  if(contador<2){
    return 0;
  }
  return 1;
  
}

int validarEdad(int edad){
  
  if(edad>130 || edad<0){
    printf("La edad debe de estar entre 0 y 130");
    return 0;
  }
  return 1;
}

int clean_stdin()
{
    while (getchar()!='\n');
    return 1;
}
